FTYPE=*.$1
TOTAL=0

for file in $(find . -name "$FTYPE") 
do
  echo $file
  LINES=$(cat $file | wc -l)
  echo $LINES
  TOTAL=$((TOTAL + LINES))
done

echo "total: "$TOTAL