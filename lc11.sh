#!/bin/bash

# recursively counts lines in files of given extension
# usage: lc10.sh file_ext


CountLines(){


TOTAL=0

for file in $1 
do
  LINES=$(cat $file | wc -l)
  echo ${file}":" $LINES
  TOTAL=$((TOTAL + LINES))
done

echo "total: "$TOTAL
}


if [ $# -eq 0 ]; then
  echo "counting all file types"
  FTYPE=$(find . -type f)
elif [ $# -eq 1 ]; then
  FTYPE=$(find . -name "*.$1")
else
  echo "usage: $0 file_extention"
  exit 1  
fi

CountLines "$FTYPE"

