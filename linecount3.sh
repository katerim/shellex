#!/bin/bash
 
excluded="git\|tmp"

countLines(){
  total=0
  for file in `find . -name "*$1" | grep -v $excluded`; do
    lines=$(cat $file | grep -v "^$" | wc -l)
    total=$((total + lines))
    echo $lines $file
  done
  echo $total "in total"
}

echo "$1 files:"
countLines $1

