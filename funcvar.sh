#!/bin/sh

fhello ()
{
   local x=3
   echo "x in function: $x"
}

x=2
echo "main x: $x"
fhello
echo "main x: $x"
echo "Done."
