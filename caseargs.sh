#!/bin/sh

#option="${1}" 
#case ${option} in 
#   -f) FILE="${2}"
#       echo "File name is $FILE";; 
#   -d) DIR="${2}"  
#       echo "Dir name is $DIR" ;; 
#   *)  echo "`basename ${0}`:usage: [-f file] | [-d directory]" 
#       exit 1 ;; 
#esac

case $1 in
  "-f") echo "Input file is $2";;
  "-d") echo "Input directory is $2";;
   *) echo "`basename $0`:usage: [-f file] | [-d directory]"
      exit 1 ;;
esac

