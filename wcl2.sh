#!/bin/bash

# wc -l wrapper
echo "counts lines in input file"
echo "usage: `basename $0` file_type"

# variables
IN=$1
EXE="wc -l"

# command
for i in *.$IN; do
  echo "$EXE $i"
  $EXE $i
done




