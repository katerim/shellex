#!/bin/bash

echo "Encrypting $1, ycx1ou'll be asked to provide a passphrase."
#echo "Enter the Exact File Name with extension"
#read file;

gpg-agent --daemon --enable-ssh-support --write-env-file "${HOME}/.gpg-agent-info"

if [ -f "${HOME}/.gpg-agent-info" ]; then
     . "${HOME}/.gpg-agent-info"
     export GPG_AGENT_INFO
     export SSH_AUTH_SOCK
     export SSH_AGENT_PID
fi


GPG_TTY=$(tty)
export GPG_TTY


#gpg -c $1

if [ $? -ne 0 ]; then 
   echo "Encryption failed, exiting."
   exit
else
   echo "Encryption was successful."
fi

