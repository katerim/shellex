#!/bin/sh
echo "0"
if [ 0 ]
 then
   echo "true"
 else
   echo "false"
fi
echo

echo "1"
if [ 1 ]
 then
   echo "true"
 else
   echo "false"
fi
echo

echo "undeclared var"
if [ $var ]
 then
   echo "true"
 else
   echo "false"
fi
echo

echo "var is null"
var=
if [ $var ]
 then
   echo "true"
 else
   echo "false"
fi
echo

echo "var=2"
var=2
if [ $var ]
 then
   echo "true"
 else
   echo "false"
fi
echo
