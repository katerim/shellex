#!/bin/bash
 
excluded="git\|tmp"

countLines(){
  total=0
  for file in `find . -name "*$1" | grep -v $excluded`; do
    lines=`cat $file | sed '/#/d' | sed '/^\s*$/d' | wc -l`
    total=$((total + lines))
    echo $lines $file
  done
  echo $total "in total"
}


if [ -z $1 ] ; then
  echo  "You must supply an argument"
  exit
fi

echo Source code files:
countLines $1

