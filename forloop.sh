#!/bin/sh

echo "sequence loop"
for j in {1..4}
do
  echo "$j"
done


for i in 1 2 3 4
do
  echo "Iteration $i."
done


echo "directory list"
for i in *.txt
do
  echo "$i"
done

echo "no argument loop"
for a
do
  echo "arg $a"
done


echo "list loop"
for planet in Mercury Venus Earth Mars Jupiter Saturn Uranus Neptune Pluto
do
  echo $planet
done

echo "list loop with quotes"
for planet in "Mercury Venus Earth Mars Jupiter Saturn Uranus Neptune Pluto"
do
  echo $planet
done
