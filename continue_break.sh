echo "continue loop"
a=0
while [ $a -lt 10 ]
do
  (( a++ ))
  if [ $a -eq 5 ] || [ $a -eq 7 ]
  then
     continue
  fi
  echo $a
done

echo "break loop"
a=0
while [ $a -lt 10 ]
do
  (( a++ ))
  if [ $a -eq 6 ]
  then
     break
  fi
  echo $a
done


