#!/bin/sh

if [ $# -ne 3 ]; then
  echo "Usage: `basename $0` integer string filename"
  exit
fi

if [ $1 -lt 0 ]
then
  echo "X is less than zero"
fi

if [ $1 -gt 0 ]; then
  echo "$1 is more than zero"
fi

if [ $1 -le 0 ]; then 
 echo "$1 is less than or equal to  zero"
fi


if [ $1 -ge 0 ]; then 
 echo "$1 is more than or equal to zero"
fi

if [ $2 = 0 ] ; then
 echo "$2 is the string or number \"0\""
fi

if [ $2 = hello ]; then 
 echo "$2 matches the string \"hello\""
fi

if [ $2 != hello ] ; then
 echo "$2 is not the string \"hello\""
fi

if [ -n $2 ]; then 
 echo "$2 is of nonzero length"
fi

if [ -f $3 ] ; then
 echo "$3 is a path of a real file" 
else
 echo "$3 does not exist."
fi

if [ -x $3 ] ; then
 echo "$3 is a path of an executable file"
fi

if [ $3 -nt "/etc/passwd" ] ; then
 echo "$3 is a file which is newer than /etc/passwd" 
fi

echo "test on \"0\""
zero=0
one=1
if [ $zero ]; then
  echo "true"
else
  echo "false"
fi



echo "arithmetic test on 0"
zero=0
one=1
if (( zero )); then
  echo "true"
else
  echo "false"
fi

echo "command test"
if touch aaa; then 
  echo "created empty file aaa"
fi


