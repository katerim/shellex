#!/bin/sh

x=0
y=0
while [ $x -lt 5 ]; do
  (( x++ ))
  echo "x   $x"
  while [ $y -lt 5 ]; do 
    (( y++ ))
    if [ $x -gt 2 ]; then
      break 2
    fi
    echo "y $y"
  done
  y=0
done
