#!/bin/sh

echo "What day is it?"
read var

case $var in 

    "Monday") echo "New week, new possibilities!";;

    "Friday") echo "TGIF!";;

    *) echo "Hang in there!";;

esac
