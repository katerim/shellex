#!/bin/bash

# recursively counts lines in files of given extension
# usage: lc.sh file_ext

CountLines(){

FTYPE=*.$1
TOTAL=0

for file in $(find . -name "$FTYPE") 
do
  echo $file
  LINES=$(cat $file | wc -l)
  echo $LINES
  TOTAL=$((TOTAL + LINES))
done

echo "total: "$TOTAL
}


if [ $# -ne 1 ]
then
  echo "usage: $0 file_extention"
  exit 1  
fi

CountLines $1

