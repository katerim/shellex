#!/bin/bash
 


countLines(){
   
  excluded="git\|tmp"
  total=0
  for file in `find . -name "*$1" | grep -v $excluded`; do
    lines=$(cat $file | grep -cv "^$")
    total=$((total + lines))
    echo $lines $file
  done
  echo $total "in total"
}


if [ $# -ne 1  ]
  then
    echo "this script recursively counts non-empty lines in files with given extension in the current directory"
    echo "usage: ./$(basename $0) file_extension"
    exit 1
  fi

echo "$1 files:"
countLines $1

