#!/bin/bash

MYARRAY=(aa bb cc dd ee ff gg hh)
echo ${MYARRAY[0]}
echo ${MYARRAY[1]}
echo ${MYARRAY[*]}
echo ${MYARRAY[@]}
echo ${MYARRAY[@]:1:4}

ARR2=(${MYARRAY[@]} 12)
echo ${ARR2[0][@]}

