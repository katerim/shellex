#!/bin/bash

# Blast+ wrapper

echo "usage: `basename $0` file output database evalue file_type"

# variables
IN=${1:-query.fa}
OUT=${2:-output}
DB=${3:-nr}
EVL=${4:-0.01}
EXE=blastp

# command
echo "$EXE -query $IN -out $OUT -db $DB -evalue $EVL"





