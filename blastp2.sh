#!/bin/bash

# Blast+ wrapper

if [ $# -ne 4 ]; then
  echo "usage: `basename $0` file_type output database evalue"
  #exit
fi


# variables
#IN=$1;OUT=$2;DB=$3;EVL=$4;EXE=blastp
IN=${1:-fa};OUT=${2:-out};DB=${3:-nr};EVL=${4:-0.01}; EXE=blastp


# command
for file in *.$IN; do 
   echo "$EXE -query $file -out $file.$OUT -db $DB -evalue $EVL"
done




