#!/bin/bash

MYVAR="I'm no longer needed."
echo "Before: $MYVAR"
unset MYVAR
echo "After: $MYVAR"

